# SPDX-License-Identifier: MIT

import time
from concurrent.futures import TimeoutError

import pytest

from snippets.timethis import timethis


def test_timethis_basic():
    with timethis() as duration:
        time.sleep(0)
    assert round(duration.result().total_seconds()) == 0


def test_timethis_only_after_exit():
    with timethis() as duration:
        with pytest.raises(TimeoutError):
            duration.result(timeout=0)
