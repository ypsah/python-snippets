# SPDX-License-Identifier: MIT

from packaging.version import Version

from snippets import __version__


def project_version():
    Version(__version__)
