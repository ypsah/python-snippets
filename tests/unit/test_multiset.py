# SPDX-License-Identifier: MIT

import pytest

from snippets.multiset import multiset


def test_multiset_init():
    multiset()


def test_multiset_init_iterable():
    multiset("python-snippets")


def test_multiset_init_mapping():
    multiset(multiset("python-snippets"))


def test_multiset_init_kwargs():
    multiset(p=3, y=1, t=2, h=1, o=1, n=2, s=2, i=1, e=1)


def test_multiset_init_non_hashable():
    with pytest.raises(TypeError):
        multiset([{}])


def test_multiset_getitem():
    bag = multiset("python-snippets")
    assert bag["p"] == 3
    assert bag["y"] == 1
    assert bag["t"] == 2
    assert bag["h"] == 1
    assert bag["o"] == 1
    assert bag["n"] == 2
    assert bag["-"] == 1
    assert bag["s"] == 2
    assert bag["i"] == 1
    assert bag["e"] == 1


def test_multiset_getitem_missing():
    assert multiset()["missing"] == 0


def test_multiset_getitem_unhashable():
    with pytest.raises(TypeError):
        multiset()[{}]


def test_multiset_iter_empty():
    assert "".join(iter(multiset())) == ""


def test_multiset_iter_non_empty():
    assert "".join(iter(multiset("python-snippets"))) == "python-sie"


def test_multiset_len_empty():
    assert len(multiset()) == 0


def test_multiset_len_non_empty():
    assert len(multiset("python-snippets")) == 10


def test_multiset_contains_in():
    assert "a" in multiset("a")


def test_multiset_contains_not_in():
    assert "a" not in multiset()


def test_multiset_contains_zeros():
    assert "a" in multiset(a=0)


def test_multiset_contains_unhashable():
    with pytest.raises(TypeError):
        {} in multiset()


def test_multiset_equality_empty():
    assert multiset() == multiset()


def test_multiset_equality_non_empty():
    assert multiset("python-snippets") == multiset("python-snippets")


def test_multiset_equality_dict():
    assert multiset("python-snippets") == dict(multiset("python-snippets"))


def test_multiset_equality_zeros():
    assert multiset() == multiset(a=0)


def test_multiset_strict_inclusion_true():
    assert multiset() < multiset(a=1)


def test_multiset_strict_inclusion_false():
    assert not multiset(a=1) < multiset()


def test_multiset_strict_inclusion_false_because_equal():
    assert not multiset() < multiset()


def test_multiset_inclusion_true():
    assert multiset() <= multiset()


def test_multiset_inclusion_false():
    assert not multiset(a=1) <= multiset()


def test_multiset_reverse_strict_inclusion_true():
    assert multiset(a=1) > multiset()


def test_multiset_reverse_strict_inclusion_false():
    assert not multiset() > multiset(a=1)


def test_multiset_strict_superset_false_because_equal():
    assert not multiset() > multiset()


def test_multiset_reverse_inclusion_true():
    assert multiset() >= multiset()


def test_multiset_reverse_inclusion_false():
    assert not multiset() >= multiset(a=1)


def test_multiset_hash_empty():
    assert hash(multiset()) == hash(multiset())


def test_multiset_hash_non_empty():
    assert hash(multiset("python-snippets")) == hash(multiset("python-snippets"))


def test_multiset_hash_non_empty_disorder():
    assert hash(multiset("python-snippets")) == hash(multiset("steppins-nohtyp"))


def test_multiset_hash_zeros():
    assert hash(multiset()) == hash(multiset(a=0))


def test_multiset_hash_empty_vs_non_empty():
    assert hash(multiset()) != hash(multiset(a=1))


def test_multiset_str_empty():
    assert str(multiset()) == "multiset()"


def test_multiset_str_non_empty():
    assert str(multiset(a=1)) == "multiset({'a': 1})"


def test_multiset_str_zeros():
    assert str(multiset(a=0)) == "multiset({'a': 0})"


def test_multiset_repr_empty():
    assert repr(multiset()) == "multiset()"


def test_multiset_repr_non_empty():
    assert repr(multiset(a=1)) == "multiset({'a': 1})"


def test_multiset_repr_zeros():
    assert repr(multiset(a=0)) == "multiset({'a': 0})"


def test_multiset_neg_empty():
    assert -multiset() == multiset()


def test_multiset_neg_zeros():
    assert -multiset(a=0) == multiset()


def test_multiset_neg_non_empty_positive_only_positive_counts_are_kept():
    assert -multiset(a=1) == multiset()


def test_multiset_neg_non_empty_negative():
    assert -multiset(a=-1) == multiset(a=1)


def test_multiset_add_empty():
    assert multiset() + multiset() == multiset()


def test_multiset_add_non_overlapping():
    assert multiset(a=1) + multiset(b=1) == multiset(a=1, b=1)


def test_multiset_add_overlapping():
    assert multiset(a=1) + multiset(a=1) == multiset(a=2)


def test_multiset_sub_empty():
    assert multiset() - multiset() == multiset()


def test_multiset_sub_non_overlapping_only_positive_counts_are_kept():
    assert multiset(a=1) - multiset(b=1) == multiset(a=1)


def test_multiset_sub_overlapping():
    assert multiset(a=1) - multiset(a=1) == multiset()


def test_multiset_intersection_empty():
    assert multiset() & multiset() == multiset()


def test_multiset_intersection_non_overlapping():
    assert multiset(a=1) & multiset(b=1) == multiset()


def test_multiset_intersection_overlapping():
    assert multiset(a=1) & multiset(a=2) == multiset(a=1)


def test_multiset_union_empty():
    assert multiset() | multiset() == multiset()


def test_multiset_union_non_overlapping():
    assert multiset(a=1) | multiset(b=1) == multiset(a=1, b=1)


def test_multiset_union_overlapping():
    assert multiset(a=1) | multiset(a=2) == multiset(a=2)


def test_multiset_elements_empty():
    assert "".join(multiset().elements()) == ""


def test_multiset_elements_non_empty_unique():
    assert "".join(multiset(a=1).elements()) == "a"


def test_multiset_elements_non_empty_duplicate():
    assert "".join(multiset(a=2).elements()) == "aa"


def test_multiset_most_common_empty():
    assert multiset().most_common() == []


def test_multiset_most_common_zeros():
    assert multiset(a=0).most_common() == [("a", 0)]


def test_multiset_most_common_non_empty_n():
    assert multiset("python-snippets").most_common(1) == [("p", 3)]


def test_multiset_most_common_non_empty_all():
    assert multiset("python-snippets").most_common(None) == [
        ("p", 3),
        ("t", 2),
        ("n", 2),
        ("s", 2),
        ("y", 1),
        ("h", 1),
        ("o", 1),
        ("-", 1),
        ("i", 1),
        ("e", 1),
    ]


def test_multiset_total_empty():
    assert multiset().total() == 0


def test_multiset_total_zeros():
    assert multiset(a=0).total() == 0


def test_multiset_total_non_empty():
    assert multiset("python-snippets").total() == 15
