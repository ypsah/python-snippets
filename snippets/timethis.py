# SPDX-License-Identifier: MIT

import datetime
import time
from collections.abc import Callable, Iterator
from concurrent.futures import Future
from contextlib import contextmanager


@contextmanager
def timethis(clock: Callable[[], float] = time.monotonic) -> Iterator[Future[datetime.timedelta]]:
    """
    Timer with a context manager interface

    >>> import time
    >>> with timethis() as duration:
    ...     time.sleep(1)
    ...
    >>> round(duration.result().total_seconds())
    1
    >>> with timethis() as duration:
    ...     duration.result(timeout=0)  # Only available after exiting the context
    ...
    Traceback (most recent call last):
      File "<stdin>", line 2, in <module>
      File "/usr/lib/python3.11/concurrent/futures/_base.py", line 458, in result
        raise TimeoutError()
    TimeoutError
    """
    start = clock()
    duration = Future()
    try:
        yield duration
    finally:
        end = clock()
        duration.set_result(datetime.timedelta(seconds=end - start))
