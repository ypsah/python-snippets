# SPDX-License-Identifier: MIT

from importlib.metadata import PackageNotFoundError, version
from pathlib import Path

from versioning.pep440 import NoVersion, Project

try:
    __version__ = version("538554a7-a272-5de8-8159-180eb5b6e62a")
except PackageNotFoundError:
    # package is not installed
    with Project(path=Path(__file__).parent) as project:
        try:
            __version__ = str(project.version())
        except NoVersion:
            __version__ = str(project.release(dev=0))
