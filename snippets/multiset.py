# SPDX-License-Identifier: MIT

import operator
from collections import Counter
from collections.abc import Hashable, Iterator, Mapping
from functools import reduce, total_ordering
from typing import Any, Optional, TypeVar

H = TypeVar("H", bound=Hashable)


@total_ordering
class multiset(Mapping[H, int]):
    """
    Frozen Counter
    """

    def __init__(self, *args, **kwargs):
        self._multiset = Counter(*args, **kwargs)

    def __getitem__(self, key: Any) -> int:
        return self._multiset[key]

    def __iter__(self) -> Iterator[H]:
        return iter(self._multiset)

    def __len__(self) -> int:
        return len(self._multiset)

    def __contains__(self, item: Any) -> bool:
        return item in self._multiset

    def __eq__(self, other: Any) -> bool:
        return self._multiset == other

    def __lt__(self, other: Any) -> bool:
        return self._multiset < other

    def __gt__(self, other: Any) -> bool:
        return self._multiset > other

    def __hash__(self) -> int:
        return hash(frozenset((key, value) for key, value in self._multiset.items() if value))

    def __str__(self) -> str:
        return str(self._multiset).replace("Counter", "multiset")

    def __repr__(self) -> str:
        return repr(self._multiset).replace("Counter", "multiset")

    def __neg__(self) -> "multiset":
        return self.__class__(-self._multiset)

    def __add__(self, other: Any) -> "multiset":
        return self.__class__(other + self._multiset)

    def __sub__(self, other: Any) -> "multiset":
        return self.__class__(self._multiset - other._multiset if isinstance(other, multiset) else other)

    def __and__(self, other: Any) -> "multiset":
        return self.__class__(other & self._multiset)

    def __or__(self, other: Any) -> "multiset":
        return self.__class__(other | self._multiset)

    def elements(self) -> Iterator[H]:
        yield from self._multiset.elements()

    def most_common(self, n: Optional[int] = None) -> list[tuple[H, int]]:
        return self._multiset.most_common(n)

    def total(self) -> int:
        return self._multiset.total()
